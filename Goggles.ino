/* Goggles.ino
   Driving my light-up goggles.

   Hardware:
   * ATTiny 841. Other Arduinos will probably work,
     but that's what I'm using.
   * 2x Neopixel rings, totallying 32 pixels. These are connected in series
     (output of the first wired to input of the second), and connected to
     pin 2 of the Arduino.

   Software:
   * FastLED (https://fastled.io). I'm currently using my own FastLED fork,
     with ATTiny 841 support. See https://github.com/FastLED/FastLED/pull/467
     for my attempt to merge it.

   License: Simplified BSD license.
   Peter Hardy <peter@hardy.dropbear.id.au>
*/
#include <FastLED.h>

const int ledPin = 2; // LEDs are attached to this pin.

const int numLeds = 32; // How many individual neopixels we have.

CRGB leds[numLeds]; // This array is our internal buffer of neopixel state.

const int numStrobers = 4; // How many bright spots will spin around
// strobeArray maintains the current position of the bright spots.
// These are indexes in to the leds array.
int strobeArray[] = { 0, 8, 16, 24 };
// How much to decrease the brightness of all LEDs per step.
// I've found 40 to be a good value for 4 strobers.
const int dropOff = 40;
// How long to pause between each step in milliseconds.
const int stepTime = 40;
void setup() {
  // Initialise the random number generator with a fairly random read.
  randomSeed(analogRead(A0));
  // INPUT_PULLUP is the recommended state for unused pins,
  // according to the Atmel datasheet. So set everything
  // to that to start.
  for (int i=0; i<12; i++) {
    pinMode(i, INPUT_PULLUP);
  }
  // Initialise FastLED. It will take care of setting the pin mode for us.
  FastLED.addLeds<WS2812B, ledPin, GRB>(leds, numLeds).setCorrection(TypicalLEDStrip);
  FastLED.setBrightness(50);
}

void loop() {
  // Start by dimming everything a fair bit.
  for (int i=0; i<numLeds; i++) {
    leds[i].subtractFromRGB(dropOff);
  }
  // Now refresh our strobers
  for (int i=0; i<numStrobers; i++) {
    leds[strobeArray[i]] = CRGB::Purple;
    strobeArray[i] = (strobeArray[i] + 1) % numLeds;
  }
  FastLED.show();
  delay(stepTime);
}
